require('dotenv').config();

module.exports = {
  /**
   * App port
   */
  serverPort: process.env.PORT || 4003,
  /**
   * App base url
   * @param {string} path
   */
  baseUrl(path = null) {
    const host = process.env.APP_URL;
    const url = `${host}:${this.serverPort}`;

    return url + (path ? `/${path}` : '');
  },

  /**
   * Api base url
   * @param {string} path
   */
  apiBaseUrl(path = null) {
    const url = `${this.baseUrl()}/api`;
    return url + (path ? `/${path}` : '');
  },
};

module.exports.GENDER = { MALE: 'male', FEMALE: 'female' };

module.exports.HTTP_INTERNAL_SERVER = 500;
module.exports.HTTP_UNPROCESSABLE = 422;
module.exports.HTTP_CONFLICT = 409;
module.exports.HTTP_NOT_FOUND = 404;
module.exports.HTTP_FORBIDDEN = 403;
module.exports.HTTP_UNAUTHORIZE = 401;
module.exports.HTTP_BAD_REQUEST = 400;
