const { GENDER } = require('../constants');

exports.up = (knex) =>
  knex.schema.createTable('users', (table) => {
    table.increments('id').unsigned().primary();
    table.string('name').nullable(false);
    table.string('phone').nullable(false);
    table.string('email', 192).unique().nullable(false);
    table.string('password');
    table.string('birthDate').nullable(false);
    table.enum('gender', [GENDER.MALE, GENDER.FEMALE]);
    table.string('address').nullable(false);
    table.timestamp('verifiedAt').nullable();
    table.integer('otp', 4).nullable();
    table.timestamp('otpExpireAt').nullable();
    table.integer('forgotPasswordCode').nullable();
    table.timestamp('forgotPasswordExpiredAt').nullable();
    table.timestamp('createdAt').defaultTo(knex.fn.now());
    table.timestamp('updatedAt').nullable();
  });

exports.down = (knex) => knex.schema.dropTable('users');
