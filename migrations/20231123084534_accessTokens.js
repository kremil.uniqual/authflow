exports.up = (knex) =>
  knex.schema.createTable("accessTokens", (table) => {
    table.string("id", 191).primary();
    table
      .integer("userId")
      .references("users.id")
      .unsigned()
      .onDelete("CASCADE");
    table.boolean("revoked").defaultTo(false);
    table.timestamp("createdAt").defaultTo(knex.fn.now());
    table.timestamp("updatedAt").defaultTo(knex.fn.now());
    table.timestamp("expiresAt");
  });

exports.down = (knex) => knex.schema.dropTable("accessTokens");
