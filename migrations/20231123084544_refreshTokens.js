exports.up = (knex) =>
    knex.schema.createTable('refreshTokens', (table) => {
        table.string('id', 191).primary();
        table.string('accessTokenId');
        table.boolean('revoked').defaultTo(false);
        table.timestamp('expiresAt');
        table.foreign('accessTokenId').references('accessTokens.id').onDelete("CASCADE");
    });

exports.down = (knex) => knex.schema.dropTable('refreshTokens');