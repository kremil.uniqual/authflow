import express from 'express';
import morgan from 'morgan';
import passport from 'passport';
import routeNotFoundHandler from './common/middlewares/not-found.handler';
import errorHandler from './common/middlewares/error.handler';
import apiRouter from './routes/api';
import './common/passport/jwt.strategy';
import knex from './common/config/database.config';

require('dotenv').config();

const app = express();

app.use(morgan('dev'));
app.use(express.json({ extend: true }));
app.use(express.urlencoded({ extended: true }));
app.use(passport.initialize());

// App API route
app.use('/api', apiRouter);

app.use(errorHandler);

app.use('*', routeNotFoundHandler);

// Server listen
app.listen(process.env.PORT, () => {
    console.log(`Listening on (HTTP/HTTPS) ${process.env.PORT}`);
});