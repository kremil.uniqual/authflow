import { hash } from 'bcrypt';
import ConflictException from '../common/exceptions/conflict-request.exception';
import knex from '../common/config/database.config';
import AuthService from './auth.service';
import UserResource from '../resource/user.resource';
import sendMail from '../common/config/mailer.config';
import moment from 'moment';
import userService from '../user/user.service';
import BadRequestException from '../common/exceptions/bad-request.exception';
import { compare } from 'bcrypt';

class AuthController {
  /**
   * Register
   * @param {*} req
   * @param {*} res
   */
  async register(req, res) {
    const checkUserExist = await knex('users')
      .where('email', req.body.email)
      .first();
    if (checkUserExist) {
      throw new ConflictException('user already registered');
    }

    const otp = Math.floor(Math.random() * 9000) + 1000;

    const [userId] = await knex('users').insert({
      name: req.body.name,
      birthDate: req.body.birthDate,
      phone: req.body.phone,
      email: req.body.email,
      password: await hash(req.body.password, 10),
      gender: req.body.gender,
      address: req.body.address,
      otp: otp,
      otpExpireAt: moment().add(10, 'minutes').format('YYYY-MM-DD HH:mm:ss'),
    });

    await sendMail({ email: req.body.email, otp }, `verification mail`);

    const user = await knex('users').where('users.id', userId).first();

    const authentication = await AuthService.generateTokenPairs(
      user.id,
      user.email,
    );

    return res.json({
      data: {
        ...new UserResource(user),
        authentication,
      },
    });
  }

  /**
   * resend otp
   * @param {*} req
   * @param {*} res
   */
  async resendOtp(req, res) {
    const otp = Math.floor(Math.random() * 9000) + 1000;

    await knex('users')
      .update({
        otp,
        otpExpireAt: moment().add(10, 'minutes').format('YYYY-MM-DD HH:mm:ss'),
      })
      .where({ email: req.body.email });

    await sendMail({ email: req.body.email, otp }, `verification mail`);

    return res.json({ message: 'otp sent successfully' });
  }

  /**
   * email verify service
   * @param {*} req
   * @param {*} res
   * @returns
   */
  async otpVerify(req, res) {
    const data = await AuthService.otpVerify(req.user, req.body);

    return res.json({ message: data });
  }

  /**
   * Login
   * @param {*} req
   * @param {*} res
   * @param {*} next
   */
  async login(req, res, next) {
    const user = await userService.findUserByEmail(req.body.email);

    if (!user || !(await compare(req.body.password, user.password))) {
      throw new BadRequestException(`Invalid email or password`);
    }

    if (user && user.verifiedAt == null) {
      throw new BadRequestException(`please verify your email first`);
    }

    const authentication = await AuthService.generateTokenPairs(
      user.id,
      user.email,
    );

    return res.json({
      data: {
        ...new UserResource(user),
        authentication,
      },
    });
  }

  /**
   * logout user
   * @param {*} req
   * @param {*} res
   * @returns
   */
  async logout(req, res) {
    await AuthService.logoutUser(req.user);

    return res.json({
      message: 'user logged out successfully',
    });
  }

  /**
   * Change password
   * @param {*} req
   * @param {*} res
   * @returns
   */
  async changePassword(req, res) {
    await AuthService.changePassword(req.user, req.body);
    return res.json({
      message: 'password changed successfully',
    });
  }
}

export default new AuthController();
