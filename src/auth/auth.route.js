import { Router } from 'express';
import asyncWrap from 'express-async-wrapper';
import AuthController from './auth.controller';
import validator from '../common/config/joi-validator.config';
import signupDto from './dto/signup.dto';
import changePasswordDto from './dto/change-password.dto';
import authenticateUser from '../common/middlewares/authenticate';
import loginDto from './dto/login.dto';
import resendOtpDto from './dto/resend-otp.dto'

const router = Router();

router.post(
  '/signup',
  validator.body(signupDto),
  asyncWrap(AuthController.register),
);

router.post('/resend-otp',validator.body(resendOtpDto),asyncWrap(AuthController.resendOtp))

router.post(
  '/otp-verify',
  authenticateUser,
  asyncWrap(AuthController.otpVerify),
);

router.post(
  '/login',
  validator.body(loginDto),
  asyncWrap(AuthController.login),
);

router.post('/logout', authenticateUser, AuthController.logout);

router.post(
  '/change-password',
  validator.body(changePasswordDto),
  authenticateUser,
  asyncWrap(AuthController.changePassword),
);

export default router;
