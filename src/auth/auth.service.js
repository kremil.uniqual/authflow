import { decode } from 'jsonwebtoken';
import { compare, hash } from 'bcrypt';
import moment from 'moment';
import ConflictHttpException from '../common/exceptions/conflict-request.exception';
import knex from '../common/config/database.config';
import AccessTokensService from '../access-tokens/access-tokens.service';
import RefreshTokensService from '../refresh-tokens/refresh-tokens.service';
import BadRequestException from '../common/exceptions/bad-request.exception';

class AuthService {
  /**
   * Generate access token & refresh token
   * @param {number} userId
   * @param {string} email
   */
  async generateTokenPairs(userId, email) {
    const accessToken = await AccessTokensService.createToken(userId, email);

    const decodedToken = decode(accessToken);

    const refreshToken = await RefreshTokensService.createToken(
      decodedToken.jti,
      decodedToken.exp,
    );

    return {
      accessToken,
      refreshToken,
      expireAt: decodedToken.exp,
    };
  }

  /**
   * email verify service
   * @param {*} body
   * @returns
   */
  async otpVerify(authUser, body) {
    if (authUser.otp !== body.otp) {
      throw new BadRequestException('invalid otp');
    }

    if (
      moment().format('YYYY-MM-DD HH:mm:ss') >
      moment(authUser.otpExpireAt).format('YYYY-MM-DD HH:mm:ss')
    ) {
      throw new BadRequestException('otp is expired');
    }

    await knex('users')
      .update({
        verifiedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      })
      .where('id', authUser.id);

    return 'email verified successfully';
  }

  /**
   * user logout
   * @param {*} authUser
   */
  async logoutUser(authUser) {
    const accessToken = await knex('accessTokens')
      .where('id', authUser.jti)
      .first();

    await knex('accessTokens').where('id', authUser.jti).update({ revoked: 1 });
    await knex('refreshTokens')
      .where('accessTokenId', accessToken.id)
      .update({ revoked: 1 });

    return;
  }

  /**
   * Change user password
   * @param {*} authUser
   * @param {*} data
   * @returns
   */
  async changePassword(authUser, data) {
    const user = await knex('users').where('id', authUser.id).first();

    if (!(await compare(data.oldPassword, user.password))) {
      throw new ConflictHttpException('Current password does not match.');
    }

    if (await compare(data.newPassword, user.password)) {
      throw new BadRequestException(
        'new password cannot be same as old password',
      );
    }

    await knex('users')
      .where('id', authUser.id)
      .update({
        password: await hash(data.newPassword, 10),
        updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      });

    return;
  }
}

export default new AuthService();
