import Joi from 'joi';

const schema = Joi.object({
  oldPassword: Joi.string().required(),
  newPassword: Joi.string().min(8).required(),
  confirmNewPassword: Joi.any().equal(Joi.ref('newPassword')).required(),
});

export default schema;
