import Joi from 'joi';
import { GENDER } from '../../../constants';

const schema = Joi.object({
  name: Joi.string().required(),
  birthDate: Joi.string().required(),
  email: Joi.string().email().required(),
  phone: Joi.number().required(),
  password: Joi.string().min(8).required(),
  confirmPassword: Joi.any().equal(Joi.ref('password')).required(),
  gender: Joi.string().valid(GENDER.MALE, GENDER.FEMALE),
  address: Joi.string().required(),
});

export default schema;
