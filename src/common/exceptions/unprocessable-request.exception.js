import { HTTP_UNPROCESSABLE } from '../../../constants';

const GeneralError = require('./general-error');

class UnprocessableException extends GeneralError {
  constructor(message) {
    super();
    this.message = message;
    this.status = HTTP_UNPROCESSABLE;
  }
}

export default UnprocessableException;
