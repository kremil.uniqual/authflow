import { createCipheriv, createDecipheriv } from 'crypto';
import bcrypt from 'bcrypt';

require('dotenv').config();

const AES_ENC_KEY = Buffer.from(process.env.AES_ENC_KEY, 'hex'); // set random encryption key
const AES_IV = Buffer.from(process.env.AES_IV, 'hex'); // set random initialisation vector

export const encrypt = (val) => {
  const cipher = createCipheriv('aes-256-cbc', AES_ENC_KEY, AES_IV);
  let encrypted = cipher.update(val, 'utf8', 'base64');
  encrypted += cipher.final('base64');
  return encrypted;
};

export const decrypt = (encrypted) => {
  const decipher = createDecipheriv('aes-256-cbc', AES_ENC_KEY, AES_IV);
  const decrypted = decipher.update(encrypted, 'base64', 'utf8');
  return decrypted + decipher.final('utf8');
};

export const encodeString = (password) => {
  const SALT = bcrypt.genSaltSync(10);
  return bcrypt.hashSync(password, SALT);
};

export const compareString = (string, hashString) =>
  bcrypt.compareSync(string, hashString);
