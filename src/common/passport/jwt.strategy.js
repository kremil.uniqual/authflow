import passport from 'passport';
import { ExtractJwt, Strategy as JwtStrategy } from 'passport-jwt';
import moment from 'moment';
import knex from '../config/database.config';

require('dotenv').config();


const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.APP_KEY,
};

passport.use(
  new JwtStrategy(options, async (jwtPayload, done) => {
    try {
      if (moment.utc().unix() > jwtPayload.exp) {
        return done(null, false);
      }

      const checkToken = await knex('accessTokens')
        .where('accessTokens.id', jwtPayload.jti)
        .where({
          userId: jwtPayload.sub,
          revoked: false,
        })
        .innerJoin('users', 'accessTokens.userId', '=', 'users.id')
        .first();

      if (
        !checkToken ||
        moment.utc().unix() > moment.unix(checkToken.expiresAt)
      ) {
        return done(null, false);
      }

      const user = await knex('users')
        .where({
          id: jwtPayload.sub,
        })
        .first();

      if (!user) {
        return done(null, false);
      }

      delete user.password;
      user.jti = jwtPayload.jti;
      return done(null, user);
    } catch (error) {
      return done(error, false);
    }
  }),
);
