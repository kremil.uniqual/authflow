import Joi from 'joi'

const schema = Joi.object({
  email: Joi.string().email().required(),
  forgotPasswordCode: Joi.number().required(),
})

export default schema
