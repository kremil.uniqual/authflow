import ForgotPasswordService from './forgot-password.service';

class ForgotPasswordController {
  /**
   * Forgot password otp send
   * @param {*} req
   * @param {*} res
   * @returns
   */
  async otpSend(req, res) {
    const data = await ForgotPasswordService.otpSend(req.body);

    return res.json({
      message: data,
    });
  }

  /**
   * Forgot password otp verify
   * @param {*} req
   * @param {*} res
   */
  async otpVerify(req, res) {
    const data = await ForgotPasswordService.otpVerify(req.body);

    return res.json({
      message: data,
    });
  }

  /**
   * Forgot password
   * @param {*} req
   * @param {*} res
   * @returns
   */
  async forgotPassword(req, res) {
    const data = await ForgotPasswordService.forgotPassword(req.body);

    return res.json({
      message: data,
    });
  }
}
export default new ForgotPasswordController();
