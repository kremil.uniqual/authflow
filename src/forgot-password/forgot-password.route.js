import { Router } from 'express';
import asyncWrap from 'express-async-wrapper';
import ForgotPasswordController from './forgot-password.controller';
import validator from '../common/config/joi-validator.config';
import verifyOtpDto from './dto/verify-otp.dto';
import forgotPasswordDto from './dto/forgot-password';

const router = Router();

router.post('/otp-send', asyncWrap(ForgotPasswordController.otpSend));

router.post(
  '/verify-otp',
  validator.body(verifyOtpDto),
  asyncWrap(ForgotPasswordController.otpVerify),
);

router.put(
  '/',
  validator.body(forgotPasswordDto),
  asyncWrap(ForgotPasswordController.forgotPassword),
);

export default router;
