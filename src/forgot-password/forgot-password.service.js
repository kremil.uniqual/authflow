import knex from '../common/config/database.config';
// import BadRequestException from '../../common/exceptions/bad-request.exception';
import sendMail from '../common/config/mailer.config';
import NotFoundException from '../common/exceptions/not-found.exception';
import moment from 'moment';
import { hash } from 'bcrypt';
import BadRequestException from '../common/exceptions/bad-request.exception';

class ForgotPasswordService {
  /**
   * Forgot password otp send
   * @param {*} body
   */
  async otpSend(body) {
    const checkUserExist = await knex('users')
      .where('email', body.email)
      .first();

    if (!checkUserExist) {
      throw new BadRequestException(
        'An account not exists with this email address',
      );
    }

    const otp = Math.floor(Math.random() * 900000) + 100000;
    await knex('users')
      .update({
        verifiedAt: null,
        forgotPasswordCode: otp,
      })
      .where('email', body.email);

    await sendMail({ otp, email: body.email }, 'forgot-password');

    return 'Please check verification code that has just been sent to your email account to change your password';
  }

  /**
   * Forgot password otp verify
   * @param {*} body
   */
  async otpVerify(body) {
    const user = await knex('users').where('email', body.email).first();

    if (user.forgotPasswordCode !== body.forgotPasswordCode) {
      throw new NotFoundException('Invalid Otp');
    }

    await knex('users')
      .update({
        verifiedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
        updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      })
      .where('email', body.email);
    return 'Otp verify successfully';
  }

  /**
   * forgot password
   * @param {*} body
   * @returns
   */
  async forgotPassword(body) {
    const user = await knex('users').where('email', body.email).first();
    if (!user) {
      throw new NotFoundException('User does not existing');
    }

    if (user && user.verifiedAt == null) {
      throw new BadRequestException('please verify your email first');
    }

    await knex('users')
      .update({
        password: await hash(body.password, 10),
      })
      .where('email', body.email);

    return 'You have Successfully changed your password';
  }
}
export default new ForgotPasswordService();
