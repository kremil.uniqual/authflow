import moment from 'moment';

require('dotenv').config();

export default class UserResource {
  constructor(data) {
    this.userId = data.id;
    this.name = data.name;
    this.email = data.email;
    this.phone = data.phone;
    this.birthDate = moment(data.birthDate, 'DD-MM-YYYY').format('DD-MM-YYYY');
    this.gender = data.gender;
    this.address = data.address;
    this.isVerified = !!data.verifiedAt;
  }
}
