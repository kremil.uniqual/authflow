import { Router } from 'express';
// import asyncWrap from 'express-async-wrapper';
import authenticateUser from '../common/middlewares/authenticate';
import authRouter from '../auth/auth.route';
import forgotPasswordRouter from '../forgot-password/forgot-password.route';
import userRouter from '../user/user.route';

const router = Router();

/**
 * APIs
 */
router.use('/v1/auth', authRouter);

router.use('/v1/forgot-password', forgotPasswordRouter);

router.use('/v1/users', authenticateUser, userRouter);

export default router;