import Joi from 'joi';
import { GENDER } from '../../../constants';

export default Joi.object({
  name: Joi.string().required(),
  birthDate: Joi.string().required(),
  email: Joi.string().email().required(),
  phone: Joi.number().required(),
  gender: Joi.string().valid(GENDER.MALE, GENDER.FEMALE),
  address: Joi.string().required(),
});
