import UserResource from '../resource/user.resource';
import UserService from './user.service';

class UserController {
  /**
   * User profile details update
   * @param {*} req
   * @param {*} res
   */
  async updateProfileDetails(req, res) {
    const profileUpdatedDetail = await UserService.updateProfileDetails(
      req.user,
      req.body,
    );

    return res.json({
      data: new UserResource(profileUpdatedDetail),
    });
  }

  /**
   * Get user profile
   * @param {*} req
   * @param {*} res
   * @returns
   */
  async userProfile(req, res) {
    const data = await UserService.userProfile(req.user);
    return res.json({
      data: new UserResource(data),
    });
  }
}
export default new UserController();
