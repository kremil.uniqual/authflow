import { Router } from 'express';
import asyncWrap from 'express-async-wrapper';
import validator from '../common/config/joi-validator.config';
import profileUpdateDto from './dto/profile-update.dto';
import UserController from './user.controller';

const router = Router();

router.put(
  '/profile',
  validator.body(profileUpdateDto),
  asyncWrap(UserController.updateProfileDetails),
);

router.get('/profile', asyncWrap(UserController.userProfile));

export default router;
