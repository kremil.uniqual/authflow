import moment from 'moment';
import { LANGUAGE, LAST_LEVEL, NEXT_STATUS_LEVEL } from '../../constants';
import knex from '../common/config/database.config';
import { deleteFile, storeAsSync } from '../common/helper';

class UserService {
  /**
   * Profile details update
   * @param {object} authUser
   * @param {object} body
   * @returns
   */
  async updateProfileDetails(authUser, body) {
    await knex('users')
      .where('id', authUser.id)
      .update({
        name: body.name,
        birthDate: body.birthDate,
        email: body.email,
        phone: body.phone,
        gender: body.gender,
        address: body.address,
        updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      });

    return knex('users').where('id', authUser.id).first();
  }

  /**
   * find user by email
   * @param {*} email
   * @returns
   */
  async findUserByEmail(email) {
    return knex('users').where('email', email).first();
  }

  /**
   * Get user profile
   * @param {*} authUser
   * @returns
   */
  async userProfile(authUser) {
    return knex('users').where('id', authUser.id).first();
  }

  /**
   * find user by provider Id and token
   * @param {string} providerType
   * @param {string} providerId
   */
  async findByProviderTypeAndId(providerType, providerId) {
    return knex('users')
      .where('providerType', providerType)
      .where('providerId', providerId)
      .first();
  }

  /**
   * create / update user
   * @param {object} data
   * @param {string} userId
   */
  async createOrUpdate(data, userId = null) {
    if (userId) {
      await knex('users').where('id', userId).update({ data });
      return knex('users').where('id', userId).first();
    }
    const [user] = await knex('users').insert({ data });
    return knex('users').where('id', user).first();
  }
}
export default new UserService();
